﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Player : MonoBehaviour {

    public static Player I;

    void Awake()
    {
        if (I == null)
            I = this;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void TileClicked(Tile t)
    {
        transform.DOMove(t.transform.position, 1f);
    }
}
