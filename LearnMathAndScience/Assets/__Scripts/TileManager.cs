﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileManager : MonoBehaviour
{
    public Tile Selected;
    public Dictionary<Point, Tile> Tiles = new Dictionary<Point, Tile>();

    public delegate void TileClicked(Tile t);
    public TileClicked TileClickedAction;

    public static TileManager I;

    void Awake()
    {
        if (I == null)
            I = this;
    }

    public void Setup(bool big)
    {
        transform.position = Vector3.zero;

        Transform[] componentsInChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in componentsInChildren)
        {
            if (child.gameObject != this.gameObject)
                Destroy(child.gameObject);
        }

        Tiles = new Dictionary<Point, Tile>();

        int x = big ? 20 : 10;
        int y = big ? 20 : 10;

        for (int k = 0; k < x; k++)
        {
            for (int l = 0; l < y; l++)
            {
                Transform newtile = Instantiate(Level2SceneManager.I.TilePrefab);
                newtile.SetParent(this.transform, false);

                if (big)
                {
                    newtile.localScale = new Vector3(.5f, .5f, 1);
                    newtile.position = new Vector3(l / 2f, k / 2f, 0);
                }
                else
                {
                    newtile.position = new Vector3(l, k, 0);
                }

                Tile tile = newtile.GetComponent<Tile>();
                tile.GridX = k;
                tile.GridY = l;
                tile.Setup();
                Tiles.Add(tile.Point, tile);
            }
        }

        this.transform.localScale = new Vector3(.85f, .85f, 1);
        if (!big)
            this.transform.localPosition = new Vector3(-5.5f, -4.2f, 0);
        else
            this.transform.localPosition = new Vector3(-5.6f, -4.3f, 0);

    }

    void OnMouseDown()
    {
        Vector3 screenToWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float minDist = 0;
        Selected = null;

        foreach (Tile t in Tiles.Values)
        {
            Vector3 dis = (t.transform.position - screenToWorldPoint);
            float planeDist = (Mathf.Abs(dis.x) + Mathf.Abs(dis.y));
            if (Selected == null || planeDist < minDist)
            {
                Selected = t;
                minDist = planeDist;
            }
        }
        TileClickedAction(Selected);
    }
}
