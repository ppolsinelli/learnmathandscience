﻿using UnityEngine;
using System.Collections;

public struct Point {

    public int X;
    public int Y;

    public Point(int gridX, int gridY)
    {
        X = gridX;
        Y = gridY;
    }
}
