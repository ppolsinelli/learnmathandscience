﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

public class Multiplications : MonoBehaviour
{
    public Tile Tile1;
    public Tile Tile2;
    public Color FilledColor1;
    public Color FilledColor2;
    private int CurrentResult;

    public DrawingState State = DrawingState.IDLE;

    public enum DrawingState
    {
        IDLE,
        FIRST_SELECTED,
        SECOND_SELECTED,
        THIRD_SELECTED,
        DRAWING,
        CLEANING_UP
    }

    public enum MultiState
    {
        IDLE,
        DRAWING
    }

    public static Multiplications I;

    void Awake()
    {
        if (I == null)
            I = this;
    }

    void OnDisable()
    {
        TileManager.I.TileClickedAction -= TileClicked;
    }

    public void TileClicked(Tile t)
    {
        if (State == DrawingState.IDLE)
        {
            foreach (Tile tile in TileManager.I.Tiles.Values)
            {
                tile.ResetColor();
            }

            Tile1 = t;
            Tile1.Sprite.color = Color.red;
            State = DrawingState.FIRST_SELECTED;

            Level2SceneManager.I.Guess.text = "";
            Level2SceneManager.I.Result.text = "";
            Level2SceneManager.I.ResultPanel.color = Color.gray;


        }
        else if (State == DrawingState.FIRST_SELECTED)
        {
            if (t.GridX == Tile1.GridX || t.GridY == Tile1.GridY)
            {
                t.Sprite.color = Color.red;
                return;
            }

            Tile2 = t;
            Tile2.Sprite.color = Color.red;
            State = DrawingState.SECOND_SELECTED;

            int lowerX = Mathf.Min(Tile1.GridX, Tile2.GridX);
            int lowerY = Mathf.Min(Tile1.GridY, Tile2.GridY);
            int upperX = Mathf.Max(Tile1.GridX, Tile2.GridX);
            int upperY = Mathf.Max(Tile1.GridY, Tile2.GridY);

            int width = upperX - lowerX + 1;
            int height = upperY - lowerY + 1;

            Level2SceneManager.I.Question.text = width + " x " + height;

            CurrentResult = width * height;

            Tile lower = TileManager.I.Tiles[new Point(lowerX, lowerY)];
            lower.Sprite.color = Color.blue;

            Tile upper = TileManager.I.Tiles[new Point(upperX, upperY)];
            upper.Sprite.color = Color.blue;

            float numOp = 1;
            int wY = lowerY;
            int wX = lowerX;
            bool useColor1 = !wY.IsEven();
            bool printedX = false;
            while (wY <= upperY)
            {
                while (wX <= upperX)
                {
                    Tile w = TileManager.I.Tiles[new Point(wX++, wY)];
                    w.Sprite.DOColor(useColor1 ? FilledColor1 : FilledColor2, numOp);
                    numOp += .1f;
                    useColor1 = !useColor1;
                }
                wY++;
                if (width.IsEven())
                    useColor1 = !useColor1;
                wX = lowerX;
            }

            if (width > 2 && height > 2)
            {
                int hint = Math.Max(width, height);
                Level2SceneManager.I.Title.text = hint + "," + hint * 2 + ", ...";
                DOVirtual.DelayedCall(2f, () =>
                {
                    Level2SceneManager.I.Title.text = "Learning :-)";
                });
            }

            State = DrawingState.IDLE;
        }
        /*else if (State == DrawingState.DRAWING)
        {
            foreach (Tile tile in TileManager.I.Tiles.Values)
            {
                tile.ResetColor();
            }
            State = DrawingState.IDLE;
        }*/

    }

    public void ValueChanged()
    {
        string answer = Level2SceneManager.I.Guess.text;
        Level2SceneManager.I.Result.text = answer;

        if (answer.Ex())
        {
            if (Int32.Parse(answer) == CurrentResult)
            {
                Level2SceneManager.I.ResultPanel.color = Color.green;
                TileManager.I.Setup(true);
            }
            else
            {
                Level2SceneManager.I.ResultPanel.color = Color.red;
            }
        }
    }
}
