﻿using System;
using UnityEngine;
using System.Collections;
using DG.Tweening;
using Random = UnityEngine.Random;

public class Tile : MonoBehaviour
{
    public int GridX;
    public int GridY;
    public Point Point;

    public SpriteRenderer Sprite;
    private Tweener colortweener;

	// Use this for initialization
	public void Setup ()
	{
	    Sprite = GetComponent<SpriteRenderer>();
        //GridX = Int32.Parse(name.Substring(name.Length - 1, 1));
        //GridY = Int32.Parse(transform.parent.name.Substring(transform.parent.name.Length - 1, 1));
	    name = "Tile " + GridX + " " + GridY;
        Point = new Point(GridX,GridY);
        ResetColor();
	}

    public void ResetColor()
    {
        if ((GridX + GridY).IsEven())
            Sprite.color = Level2SceneManager.I.ChessWhite;
        else
            Sprite.color = Level2SceneManager.I.ChessBlack;
    }

    // Update is called once per frame
	void Update ()
	{
	    /*if (colortweener == null || !colortweener.IsActive())
	    {
	        colortweener = Sprite.DOColor(Color.green, Random.Range(3.0f, 4.0f)).OnComplete(() =>
	        {
                colortweener = Sprite.DOColor(Color.gray, Random.Range(3.0f, 4.0f));
	        });
	    }*/
	}
}
