﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Level2SceneManager : MonoBehaviour
{
    [Header("Scene elements")]

    public RectTransform Splash;
    public RectTransform InputResultTable;
    public RectTransform Settings;

    public Transform Grid;

    public Text Title;

    public Text Question;
    public InputField Guess;
    public Image ResultPanel;
    public Text Result;

    public Color ChessWhite;
    public Color ChessBlack;

    [Header("Prefabs")]

    public Transform TilePrefab;

    [Header("Others")]
    public Multiplications.DrawingState State = Multiplications.DrawingState.IDLE;

    bool setupped;

    public static Level2SceneManager I;

    void Awake()
    {
        if (I == null)
            I = this;
    }

    void Update()
    {
        if (!setupped)
        {
            TileManager.I.Setup(false);
            setupped = true;
            TileManager.I.TileClickedAction += Multiplications.I.TileClicked;
            TileManager.I.TileClickedAction += Player.I.TileClicked;
        }
    }
}
